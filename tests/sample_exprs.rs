/*!
Test round-trip parsing of sample `rain` expressions
*/
use rain_ast::ast::*;
use rain_ast::parser::*;

fn is_ws(s: &str) -> bool {
    if s == "" {
        return true;
    }
    match cws(s) {
        Ok((rest, _)) => rest == "",
        Err(_) => false,
    }
}

fn round_trip_parse_test(input: &str) {
    let (rest, parsed) = match parse_expr(input) {
        Ok(result) => result,
        Err(err) => panic!(
            "Failed to parse input string {:?}: got error {:?}",
            input, err
        ),
    };
    assert!(
        is_ws(rest),
        "Failed to parse entire input string {:?} (rest = {:?})",
        input,
        rest
    );
    let printed = format!("{}", parsed);
    let (rest, re_parsed) = if let Ok(result) = parse_expr(&printed) {
        result
    } else {
        panic!(
            "Failed to parse Display output {:?} for input {:?}",
            printed, input
        )
    };
    assert!(
        is_ws(rest),
        "Failed to parse entire Display output for {:?} (rest = {:?})",
        input,
        rest
    );
    // Dirty dirty hack because I'm too lazy to make PartialEq lifetime-generic...
    let parsed: Expr = unsafe { std::mem::transmute(parsed) };
    assert_eq!(
        parsed, re_parsed,
        "Display output for input string {:?} parses incorrectly",
        input
    );
}

#[test]
fn logical_examples() {
    let logical_examples = [
        "#and",
        "#not",
        "(#and #true #false)",
        "(#or (#and #true #false) (#xor #true #false))",
        "(#not #true #false)", // This is a semantic error, but *not* a syntax error!
    ];
    for example in logical_examples.iter() {
        round_trip_parse_test(example)
    }
}

#[test]
fn lambda_examples() {
    let lambda_examples = [
        "|x: #bool| x",
        "|x: #finite(3)| #bool",
        "|s: #bool h: #bool l: #bool| (#or (#and s h) (#and (#not s) l))",
    ];
    for example in lambda_examples.iter() {
        round_trip_parse_test(example)
    }
}

#[test]
fn gamma_examples() {
    let gamma_examples = [
        "
        #gamma |#bool| {
            #true => #false,
            #false => #true
        }
        ",
        "
        #gamma |#finite(3)| {
            #ix(3)[0] => #false,
            #ix(3)[2] => #false,
            #ix(3)[1] => #true,
        }
        ",
        // Mixing branch types and incomplete gamma nodes are semantic errors, *not* syntax errors!
        "
        #gamma |#finite(5)| {
            #ix(5)[3] => #ix(7)[6],
            #true => #ix(2)[0],
            #ix(8)[1] => #false
        }
        ",
    ];
    for example in gamma_examples.iter() {
        round_trip_parse_test(example)
    }
}
