/*!
Tokens representing primitive `rain`-types
*/

use crate::tokens::*;
use crate::{debug_from_display, quick_display};
use std::fmt::{self, Display, Formatter};

/// The unit type
#[derive(Copy, Clone, Eq, PartialEq, Hash)]
pub struct Unit;

quick_display!(Unit, "#unit");
debug_from_display!(Unit);

/// The empty type
#[derive(Copy, Clone, Eq, PartialEq, Hash)]
pub struct Empty;

quick_display!(Empty, "#empty");
debug_from_display!(Empty);

/// A type with `n` values
#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash)]
#[repr(transparent)]
pub struct Finite(pub u128);

debug_from_display!(Finite);
quick_display!(Finite, s, fmt => write!(fmt, "{}({})", KEYWORD_FINITE, s.0));

/// The type of booleans
#[derive(Clone, Copy, Eq, PartialEq, Hash)]
pub struct Bool;

debug_from_display!(Bool);
quick_display!(Bool, "{}", KEYWORD_BOOL);

/// Masks corresponding to what bits must be set for operations of a given arity
pub const LOGICAL_OP_ARITY_MASKS: [u128; 8] = [
    0b1,                                // Nullary
    0b11,                               // Unary
    0xF,                                // Binary
    0xFF,                               // Ternary
    0xFFFF,                             // Arity 4
    0xFFFFFFFF,                         // Arity 5,
    0xFFFFFFFFFFFFFFFF,                 // Arity 6
    0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF, // Arity 7
];

/// A boolean operation, operating on up to seven booleans
#[derive(Copy, Clone, PartialEq, Eq, Hash)]
pub struct Logical {
    /// The data backing this logical operation
    data: u128,
    /// The arity of this logical operation
    arity: u8,
}

impl Logical {
    /// Create a new logical operation with a given type and data set.
    /// Return an error if the arity is zero, or greater than seven, or
    /// if there are nonzero bits corresponding to higher arities
    #[inline]
    pub fn try_new(arity: u8, data: u128) -> Result<Logical, ()> {
        if arity == 0 || arity > 7 || !LOGICAL_OP_ARITY_MASKS[arity as usize] & data != 0 {
            Err(())
        } else {
            Ok(Logical { arity, data })
        }
    }
    /// Create a constant logical operation with a given arity.
    /// Return an error if the arity is zero, or greater than seven
    #[inline]
    pub fn try_const(arity: u8, value: bool) -> Result<Logical, ()> {
        if arity == 0 || arity > 7 {
            Err(())
        } else {
            Ok(Logical {
                arity,
                data: if value {
                    LOGICAL_OP_ARITY_MASKS[arity as usize]
                } else {
                    0
                },
            })
        }
    }
    /// Get the arity of this logical value
    #[inline]
    pub fn arity(&self) -> u8 {
        self.arity
    }
    /// Get the data of this logical value
    #[inline]
    pub fn data(&self) -> u128 {
        self.data
    }
}

impl Display for Logical {
    fn fmt(&self, fmt: &mut Formatter) -> Result<(), fmt::Error> {
        match self.arity {
            1 => write!(
                fmt,
                "{}({}, {:#04b})",
                KEYWORD_LOGICAL, self.arity, self.data
            ),
            2 => write!(
                fmt,
                "{}({}, {:#06b})",
                KEYWORD_LOGICAL, self.arity, self.data
            ),
            3 => write!(
                fmt,
                "{}({}, {:#010b})",
                KEYWORD_LOGICAL, self.arity, self.data
            ),
            _ => write!(fmt, "{}({}, {:#x})", KEYWORD_LOGICAL, self.arity, self.data),
        }
    }
}

debug_from_display!(Logical);

/// The logical identity operation
#[derive(Copy, Clone, Eq, PartialEq, Hash)]
pub struct LogicalId;
debug_from_display!(LogicalId);
quick_display!(LogicalId, "{}", KEYWORD_LOGICAL_ID);

/// The logical not operation
#[derive(Copy, Clone, Eq, PartialEq, Hash)]
pub struct Not;
debug_from_display!(Not);
quick_display!(Not, "{}", KEYWORD_NOT);

/// The logical and operation
#[derive(Copy, Clone, Eq, PartialEq, Hash)]
pub struct And;
debug_from_display!(And);
quick_display!(And, "{}", KEYWORD_AND);

/// The logical or operation
#[derive(Copy, Clone, Eq, PartialEq, Hash)]
pub struct Or;
debug_from_display!(Or);
quick_display!(Or, "{}", KEYWORD_OR);

/// The logical xor operation
#[derive(Copy, Clone, Eq, PartialEq, Hash)]
pub struct Xor;
debug_from_display!(Xor);
quick_display!(Xor, "{}", KEYWORD_XOR);

/// The logical nor operation
#[derive(Copy, Clone, Eq, PartialEq, Hash)]
pub struct Nor;
debug_from_display!(Nor);
quick_display!(Nor, "{}", KEYWORD_NOR);

/// The logical nand operation
#[derive(Copy, Clone, Eq, PartialEq, Hash)]
pub struct Nand;
debug_from_display!(Nand);
quick_display!(Nand, "{}", KEYWORD_NAND);

/// The logical equivalence operation, i.e. iff/xnor/nxor
#[derive(Copy, Clone, Eq, PartialEq, Hash)]
pub struct Iff;
debug_from_display!(Iff);
quick_display!(Iff, "{}", KEYWORD_IFF);

/// Bitvector addition
#[derive(Copy, Clone, Eq, PartialEq, Hash)]
pub struct Add;
debug_from_display!(Add);
quick_display!(Add, "{}", KEYWORD_ADD);

/// Bitvector subtraction
#[derive(Copy, Clone, Eq, PartialEq, Hash)]
pub struct Sub;
debug_from_display!(Sub);
quick_display!(Sub, "{}", KEYWORD_SUB);

/// Bitvector multiplication
#[derive(Copy, Clone, Eq, PartialEq, Hash)]
pub struct Mul;
debug_from_display!(Mul);
quick_display!(Mul, "{}", KEYWORD_MUL);

/// Bitvector negation
#[derive(Copy, Clone, Eq, PartialEq, Hash)]
pub struct Neg;
debug_from_display!(Neg);
quick_display!(Neg, "{}", KEYWORD_NEG);

/// Bits literal
#[derive(Copy, Clone, Eq, PartialEq, Hash)]
pub struct Bits {
    /// value of this bitvector
    pub data: u128,
    /// len of this bitvector
    pub len: u32,
}
debug_from_display!(Bits);
quick_display!(Bits, s, fmt => write!(fmt, "{}'h{:x}", s.len, s.data));
