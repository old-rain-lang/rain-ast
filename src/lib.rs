/*!
The grammar and parser for the `rain` language.

Note that this does *not* include a prettyprinter for the intermediate representation: due to restrictions on cyclic dependencies,
that's part of the main `rain-ir` crate instead, as the prettyprinter is used for `Debug`/`Display` implementations.
*/
#![deny(missing_docs, unsafe_code, missing_debug_implementations)]
#![warn(clippy::all)]

#[cfg(feature = "ast")]
pub mod ast;
#[cfg(feature = "parser")]
pub mod parser;
pub mod tokens;

/// Implement `Debug` for a type which implements `Display`
#[macro_export]
macro_rules! debug_from_display {
    ($t:ty) => {
        impl std::fmt::Debug for $t {
            fn fmt(&self, fmt: &mut std::fmt::Formatter) -> Result<(), std::fmt::Error> {
                std::fmt::Display::fmt(self, fmt)
            }
        }
    };
}

/// Quickly implement `Display` using a given function or format string
#[macro_export]
macro_rules! quick_display {
    ($t:ty, $s:pat, $fmt:pat => $e:expr) => {
        impl std::fmt::Display for $t {
            fn fmt(&self, fmt: &mut std::fmt::Formatter) -> Result<(), std::fmt::Error> {
                let $s = self;
                let $fmt = fmt;
                $e
            }
        }
    };
    ($t:ty, $fmt_string:literal $(, $e:expr)*) => {
        $crate::quick_display!($t, _, fmt => write!(fmt, $fmt_string $(, $e)*));
    };
}
